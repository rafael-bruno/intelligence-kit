# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.api as sm

from scikit_posthocs import posthoc_nemenyi_friedman
from scipy.stats import friedmanchisquare

from sklearn.feature_selection import f_regression

sns.set()

class Statistics():

  def __init__(self, processed_data):
    self.X = processed_data.X
    self.y = self._get_y(processed_data)
    return
  
  def _get_y(self, processed_data):
    if processed_data.y is not None:
      return processed_data.y.reshape(1, -1)[0] 
    else:
      return []

  def check_correlation_between_variables(self):
    """
		Correlation between variables

		0.0         - does not exists
		(0.0, 0.5]  - weak
		(0.5, 0.7]  - moderate
		(0.7, 1.0)  - strong
		1.0         - perfect
		"""
    # correlation = np.corrcoef(self.X, self.y, rowvar=False)
    # print(f'Correlation between variables:\n{correlation}', end='\n')
    return

  def evaluate_feature_selection(self) -> None:
    """
    If a variable has a p-value > 0.05, you can disregard it
    """
    if len(self.X) and len(self.y):
      f_statistics, p_values = f_regression(self.X, self.y)

      print(f'F-statistics:\n {f_statistics}', end='\n')
      print(f'p-value:\n {p_values.round(4)}', end='\n\n')
    else:
      print(f'Not enough values to do feature selection', end='\n')
    return

  def nemenyi_friedman(self, trained_models_scores) -> None:
    try:
      f_statistics, p_value = friedmanchisquare(*trained_models_scores.values.T)
      print(f'[Friedman]')
      print(f'F-statistics: {f_statistics}', end='\n')
      print(f'p-value: {p_value}', end='\n\n')

      print(f'[Nemenyi Friedman]')
      nemenyi_friedman_dataframe = posthoc_nemenyi_friedman(trained_models_scores)
      print(nemenyi_friedman_dataframe.to_string(), end='\n\n')
    except Exception as error:
      print(f'Unable to execute nemenyi_friedman because of the following error:\n  - {error}')
    return
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from pyod.models import knn

from sklearn.compose import ColumnTransformer
from sklearn.decomposition import KernelPCA, PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.impute import SimpleImputer
from sklearn.model_selection import KFold, ShuffleSplit, StratifiedKFold
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, StandardScaler

from typing import List

sns.set()

class PreProcessor():
	"""
	Data cleansing
		- drop_unnecessary_data
		- rearrange_data
		- check_missing_values
		- deal_with_missing_values
		- check_inconsistent_values
		- deal_with_inconsistent_values
		- check_duplicate_values (TO DO)
		- deal_with_duplicate_values (TO DO)
		- check_outliers_values
		- deal_with_outliers_values
		- format_data
	
	Feature Construction
		- create_new_feature

	Feature Extraction
		- reduce_dimensionality
	
	Feature Selection
		- select_features

	Instances Selection and Partitioning
		- split_dataset_into_X_and_y
		- set_train_validation_test_indices

	Feature Tuning
		- standardize_data
		- one_hot_encode_data
		- binary_encode_data
	"""

	def __init__(self, raw_data):
		self.raw_data = raw_data
		self.n_rows, self.n_columns = None, None
		self.X, self.y = None, None
		self.scaler_X, self.scaler_y = None, None
		self.train_validation_test_indices = []
		self.n_tests = None
		self.test_splits = None
		return
	
	## Data cleansing

	def drop_unnecessary_data(self, columns_names: List[str] = None, axis_name: str = None) -> pd.DataFrame:
		"""
		This method must be called even if there's no data to drop
		"""
		axis = { 'row': 0, 'column': 1 }
		if columns_names and axis_name:
			self.raw_data = self.raw_data.drop(columns_names, axis=axis[axis_name])
		self.n_rows, self.n_columns = self.raw_data.shape
		return self.raw_data
	
	def rearrange_data(self, columns_names: List[str] = None) -> pd.DataFrame:
		self.raw_data = self.raw_data[columns_names]
		return self.raw_data

	def check_missing_values(self) -> None:
		print('[Check missing values]')
		print(self.raw_data.isnull().sum(), end='\n')
		return
	
	def deal_with_missing_values(self,
															drop_missing_values: bool = False,
															missing_values=np.nan,
															strategy: str = 'mean',
															columns_names: List[str] = None) -> pd.DataFrame:
		"""
		If you are removing < 5% of the observations, you are free to just
		remove all that have missing values
		"""

		# if missing values are < 5% of the observations
		if drop_missing_values:
			self.raw_data = self.raw_data.dropna(axis=0)

		# if missing values are >= 5% of the observations
		imputer = SimpleImputer(missing_values=missing_values, strategy=strategy)
		if columns_names:
		  self.raw_data.loc[:, columns_names] = imputer.fit_transform(self.raw_data.loc[:, columns_names].values)
		else:
			self.raw_data.loc[:,:] = imputer.fit_transform(self.raw_data.loc[:,:].values)

		return self.raw_data
	
	def check_inconsistent_values(self) -> None:
		# this method should be overrided
		print('[Check inconsistent values]')
		return
	
	def deal_with_inconsistent_values(self) -> pd.DataFrame: 
		# this method should be overrided
		return self.raw_data

	def check_duplicate_values(self) -> None:
		# to do
		print('[Check duplicate values]')
		return
	
	def deal_with_duplicate_values(self) -> pd.DataFrame:
		# to do
		return self.raw_data

	def check_outliers(self, plot_columns: List[str]) -> None:
		for i, column in enumerate(self.raw_data.columns):
		  if column in plot_columns:
		    plt.figure(i+1, figsize=(10, 10))
		    # plt.boxplot(self.raw_data[column])
		    sns.distplot(self.raw_data[column], kde_kws={'bw':0.1})
		plt.show()
		return
	
	def deal_with_outliers(self,
												top_outliers_columns: List[str] = [],
												bottom_outliers_columns: List[str] = []) -> pd.DataFrame:
		"""
		If there are outliers, they will affect the regression.
		One way to deal with them is to remove the top/bottom 1% outliers
		(choosing the top or bottom will depend on the data)
		"""
		if len(top_outliers_columns) or len(bottom_outliers_columns):
			for column in self.raw_data.columns:
				if column in top_outliers_columns: # Remove top 1%
					self.raw_data = self.raw_data[self.raw_data[column] < self.raw_data[column].quantile(0.99)]
				if column in bottom_outliers_columns: # Remove bottom 1%
					self.raw_data = self.raw_data[self.raw_data[column] > self.raw_data[column].quantile(0.01)]

		outlier_detector = knn.KNN()
		outlier_detector.fit(self.raw_data.iloc[:, 0:self.n_columns-1])
		outliers = np.where(outlier_detector.labels_ == 1)
		self.raw_data = self.raw_data.drop(outliers[0])
		return self.raw_data
	
	def format_data(self, columns_names: List[str] = None, format_type: str = None) -> pd.DataFrame:
		if format_type == 'date':
			self.raw_data[columns_names] = pd.to_datetime(self.raw_data[columns_names])

		return self.raw_data


	## Feature Construction

	def create_new_feature(self):
		# this method should be overrided
		return


	## Feature Extraction

	def reduce_dimensionality(self,
														technique: str = None,
														n_components: int = None,
														kernel: str = 'rbf'):
		"""
		Used when there are too many information to be processed
		"""
		# Unsupervised
		# PCA       - linear problems
		# KernelPCA - non-linear problems

		if technique == 'pca':
			pca = PCA(n_components=n_components)
			self.X = pca.fit_transform(self.X)
			print(f'Variance Ratio:\n{pca.explained_variance_ratio_}')
		
		if technique == 'kernel_pca':
			kpca = KernelPCA(n_components=n_components, kernel=kernel)
			self.X = kpca.fit_transform(self.X)

		# Supervised for classification problems

		elif technique == 'lda':
			lda = LinearDiscriminantAnalysis(n_components=n_components)
			self.X = lda.fit_transform(self.X, self.y)
		return self.X


	## Feature Selection

	def select_features(self):
		# to do
		return
	

	## Feature Tuning

	def standardize_data(self, X = None, y = None):
		"""
		Used in numeric values
		"""
		if not X:
			X = self.X
		
		if not y:
			y = self.y
		
		self.scaler_X = StandardScaler()
		self.scaler_y = StandardScaler()

		if X is not None:
			self.X = self.scaler_X.fit_transform(X)

		if y is not None:
			self.y = self.scaler_y.fit_transform(y)
		return self.scaler_X, self.scaler_y
	
	def one_hot_encode_data(self, columns_to_encode: List[int] = None):
		"""
		Used in categorical values when there are a few categories
		"""
		columns_index = []
		label_encoder_X = LabelEncoder()

		for column in columns_to_encode:
			index = np.where(self.raw_data.columns.values == column)[0][0]
			columns_index.append(index)
			self.X[:, index] = label_encoder_X.fit_transform(self.X[:, index])

		one_hot_encoder = ColumnTransformer([('one_hot_encoder', OneHotEncoder(), columns_index)], remainder='passthrough')
		self.X = one_hot_encoder.fit_transform(self.X).toarray()

		label_encoder_y = LabelEncoder()
		self.y = label_encoder_y.fit_transform(self.y.ravel())

		return self.X, self.y
	
	def binary_encode_data(self):
		"""
		Used in categorical values when there are many categories
		"""

		return


	## Instances Selection and Partitioning

	def split_dataset_into_X_and_y(self,
																X_columns_names: List[str] = [],
																y_columns_names: List[str] = []):
		if len(X_columns_names) or len(y_columns_names):
			if len(X_columns_names):
				self.X = self.raw_data.loc[:, X_columns_names].values
			if len(y_columns_names):
				self.y = self.raw_data.loc[:, y_columns_names].values
		else:
			self.X = self.raw_data.iloc[:, 0:self.n_columns-1].values
			self.y = self.raw_data.iloc[:, self.n_columns-1:self.n_columns].values
		return self.X, self.y

	def set_train_validation_test_indices(self,
																			kfold_strategy: str = 'StratifiedKFold',
																			n_tests: int = 30,
																			test_split_size: float = 0.1,
																			validation_split_size: float = 0.2):
		self.n_tests = n_tests
		self.test_splits = int(1/test_split_size)
		for i in range(n_tests):
			if kfold_strategy == 'StratifiedKFold':
				kfold = StratifiedKFold(n_splits=self.test_splits, shuffle=True, random_state=i)
			elif kfold_strategy == 'KFold':
				kfold = KFold(n_splits=self.test_splits, shuffle=True, random_state=i)
			
			for train_validation_index, test_index in kfold.split(self.X, self.y):
				if validation_split_size > 0:
					shuffle_split = ShuffleSplit(n_splits=1, test_size=validation_split_size, random_state=0)
					split = [(t_i, v_i) for t_i, v_i in shuffle_split.split(self.X[train_validation_index])][0]

					train_index = np.take(train_validation_index, split[0])
					validation_index = np.take(train_validation_index, split[1])
				else:
					train_index = train_validation_index
					validation_index = []

				self.train_validation_test_indices.append((train_index, validation_index, test_index))
		
		return self.train_validation_test_indices

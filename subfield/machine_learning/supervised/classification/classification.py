# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from collections import Counter

from sklearn.metrics import accuracy_score, confusion_matrix

from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

from techniques.pre_processing.pre_processing import PreProcessor
from techniques.statistics.statistics import Statistics

from types import SimpleNamespace

sns.set()

"""
#Analytics

##Models

###Supervised Learning
- Naive-Bayes
- Decision Trees
- Random Forests
- K-Nearest Neighbors
- Logistic Classification
- Suport Vector Machines
- Artificial Neural Networks
"""

class ClassificationModel():

  def __init__(self, processed_data):
    self.X = processed_data.X
    self.y = processed_data.y.reshape(1, -1)[0]
    self.scaler_X = processed_data.scaler_X
    self.scaler_y = processed_data.scaler_y
    self.train_validation_test_indices = processed_data.train_validation_test_indices
    self.n_tests = processed_data.n_tests
    self.test_splits = processed_data.test_splits
    self.classifiers_models = []
    self.trained_models = {}
    self.training_data = {}
    return
  
  def _map_dict(self, original_dict, map_by):
    return { model: list(map(lambda d: d[map_by], original_dict[model]))
                    for model in original_dict.keys() }

  def naive_bayes_classifier(self):
    model = {
      'classifier': GaussianNB(),
      'name': 'Naive-Bayes Classifier'
    }
    return SimpleNamespace(**model)
  
  def decision_tree_classifier(self, criterion='entropy'):
    model = {
      'classifier': DecisionTreeClassifier(criterion=criterion),
      'name': 'Decision Tree Classifier'
    }
    return SimpleNamespace(**model)
  
  def random_forest_classifier(self, n_estimators=None, criterion='entropy'):
    model = {
      'classifier': RandomForestClassifier(n_estimators=n_estimators, criterion=criterion),
      'name': 'Random Forest Classifier'
    }
    return SimpleNamespace(**model)
  
  def knn_classifier(self, n_neighbors=None, metric='minkowski', p=2):
    model = {
      'classifier': KNeighborsClassifier(
        n_neighbors=n_neighbors,
        metric=metric,
        p=p
      ),
      'name': 'KNN Classifier'
    }
    return SimpleNamespace(**model)

  def logistic_classifier(self, solver='liblinear'):
    """
    Used in non-linear problems. Usually in problems with two categories
    """

    model = {
      'classifier': LogisticRegression(solver=solver),
      'name': 'Logistic Classifier'
    }
    return SimpleNamespace(**model)

  def suport_vector_classifier(self, C=1.0, kernel='rbf', degree=3, gamma='scale'):
    model = {
      'classifier': SVC(C=C, kernel=kernel, degree=degree, gamma=gamma),
      'name': 'Suport Vector Classifier'
    }
    return SimpleNamespace(**model)
  
  def multilayer_perceptron_classifier(
    self,
    solver='adam',
    max_iter=10000,
    tol=0.00001,
    hidden_layer_sizes=(100),
    learning_rate_init=0.001,
    activation='relu'):
    """
    In regression's problems we only use one neuron in the output layer
    and that neuron must have a linear activation function.
    """

    model = {
      'classifier': MLPClassifier(
        solver=solver,
        max_iter=max_iter,
        tol=tol,
        hidden_layer_sizes=hidden_layer_sizes,
        learning_rate_init=learning_rate_init,
        activation=activation
      ),
      'name': 'Multilayer Perceptron Classifier'
    }
    return SimpleNamespace(**model)

  def train_models(self):
    for model in self.classifiers_models:
      self.trained_models[model.name] = []

      for i in range(self.n_tests):
        
        for train_index, validation_index, test_index in self.train_validation_test_indices:
          model.classifier.fit(self.X[train_index], self.y[train_index])
          train_score = model.classifier.score(self.X[train_index], self.y[train_index])
          test_score = model.classifier.score(self.X[test_index], self.y[test_index])
          
          #y_test = self.scaler_y.inverse_transform(self.y[test_index])
          
          predictions = model.classifier.predict(self.X[test_index])
          #predictions = self.scaler_y.inverse_transform(predictions)

          self.trained_models[model.name].append({
            'classifier': model.classifier,
            'score': round((train_score + test_score)/2, 4),
            'accuracy_score': accuracy_score(self.y[test_index], predictions),
            'confusion_matrix': confusion_matrix(self.y[test_index], predictions)
          })
        print(f'{model.name}: {(i+1)*self.test_splits}/{self.n_tests*self.test_splits}')

      self.training_data[model.name] = {
        'best_model': np.argmax(list(map(lambda td: td['score'],self.trained_models[model.name]))),
        'mean_score': round(np.mean(list(map(lambda td: td['score'], self.trained_models[model.name]))), 4)
      }
    
    self.training_data = pd.DataFrame(self.training_data)
    return self.trained_models, self.training_data
  
  def rank_models(self):
    self.trained_models_scores = pd.DataFrame(self._map_dict(self.trained_models, 'score'))
    self.ranking_data = pd.DataFrame(self._map_dict(self.trained_models, 'score'))

    for i in range(self.ranking_data.shape[0]):
      row = self.ranking_data.iloc[i, :]
      sorted_row = sorted(row, reverse=True)
      self.ranking_data.iloc[i, :] = [sorted_row.index(item) + 1 for item in row]
    return self.trained_models_scores, self.ranking_data
  
  def get_best_model(self):
    ranking_mean = np.asarray(self.ranking_data.mean())
    best_ranked_model = np.where(ranking_mean == min(ranking_mean))[0][0]
    model_name = self.ranking_data.columns[best_ranked_model]
    best_model_index = int(self.training_data[model_name]['best_model'])
    return self.trained_models[model_name][best_model_index]
  
  def get_base_line_classifier(self):
    counter = Counter(self.y)
    counter = list(counter.values())
    self.base_line_classifier = round(max(counter)/sum(counter), 4)

    print(f'Base line classifier: {self.base_line_classifier}', end='\n\n')
    return

  def plot_model(self, classifier, chart_settings):
    X = self.X
    y = self.y

    if self.X.shape == self.y.shape:
      if 'Decision Tree' in classifier.name or 'Random Forest' in classifier.name:
        X = np.arange(min(X), max(X), 0.1)
        X = X.reshape(-1, 1)
      
      plt.scatter(X, y, color='blue')
      plt.plot(X, classifier.predict(X), color='red')
      plt.title(classifier['name'])
      plt.xlabel(chart_settings['x_label'])
      plt.ylabel(chart_settings['y_label'])
    
    else:
      print(f'Unable to plot because X.shape is {X.shape} and y.shape is {y.shape}')
    return


class ThisPreProcessor(PreProcessor):

  def __init__(self, raw_data):
    super().__init__(raw_data)
  
  def deal_with_inconsistent_values(self):
    valid_range = (self.raw_data['age']) > 0 & (self.raw_data['age'] < 120)
    invalid_range = (self.raw_data['age']) < 0 | (self.raw_data['age'] > 120)
    
    # this will remove the data outside the range 0 < age < 120
    # self.raw_data = self.raw_data[valid_range]

    # this will substitute the data outside the range 0 < age < 120
    self.raw_data.loc[self.raw_data['age'][invalid_range], 'age'] = self.raw_data['age'][valid_range].mean()
    return

"""
Main
"""
if __name__ == '__main__':
  #Load Data
  raw_data = pd.read_csv('data/census.csv')
  print(raw_data.describe(include='all'), end='\n')

  # Preprocessing
  pp = ThisPreProcessor(raw_data)

  ## Data cleansing
  pp.drop_unnecessary_data()
  # pp.rearrange_data()
  pp.check_missing_values()
  # pp.deal_with_missing_values()
  pp.check_inconsistent_values()
  pp.deal_with_inconsistent_values()
  # pp.check_duplicate_values()
  # pp.deal_with_duplicate_values()
  # pp.check_outliers(plot_columns=pp.raw_data.columns)
  # pp.deal_with_outliers()
  # pp.format_data()

  ## Feature Construction
	# pp.create_new_feature()

  ## Feature Extraction
  # pp.reduce_dimensionality(technique='pca', n_components=None)
  
  ## Instances Selection and Partitioning
  pp.split_dataset_into_X_and_y()
  pp.set_train_validation_test_indices(kfold_strategy='StratifiedKFold',
																			n_tests=3,
																			test_split_size=0.1,
																			validation_split_size=0)

  ## Feature Tuning
  pp.one_hot_encode_data(columns_to_encode=[
    'workclass', 'education', 'marital-status', 'occupation',
    'relationship', 'race', 'sex', 'native-country'
  ])
  # pp.standardize_data(X=pp.X.reshape(-1,1), y=pp.y.reshape(-1,1))

  ## Feature Selection
  # statistics = Statistics(pp)
  # statistics.check_correlation_between_variables()
  # statistics.evaluate_feature_selection()
  # pp.select_features()

  """
  #Analysis

  - Use data to create reports and dashboards to gain business insights
  - Extract info and present it in form of: metrics, KPIs, reports, dashboards, etc.
  - Use PowerBI/Tableau/Google Data Studio
  """

  classifier = ClassificationModel(pp)
  classifier.classifiers_models = [
    classifier.naive_bayes_classifier(),
    # classifier.decision_tree_classifier(criterion='entropy'),
    # classifier.random_forest_classifier(n_estimators=10, criterion='entropy'),
    # classifier.knn_classifier(n_neighbors=5, metric='minkowski', p=2),
    # classifier.logistic_classifier(solver='liblinear'),
    # classifier.suport_vector_classifier(C=1.0, kernel='rbf', degree=3, gamma='scale'), # to use 'rbf' is necessary to scale the data
    # classifier.multilayer_perceptron_classifier(
    #   solver='adam',
    #   max_iter=10000,
    #   tol=0.00001,
    #   hidden_layer_sizes=(100),
    #   learning_rate_init=0.001,
    #   activation='relu')
  ]
  trained_models, training_data = classifier.train_models()
  trained_models_scores, ranking_data = classifier.rank_models()
  model = classifier.get_best_model()

  training_data = pd.DataFrame(training_data)
  trained_models_scores = pd.DataFrame(trained_models_scores)
  ranking_data = pd.DataFrame(ranking_data)

  classifier.get_base_line_classifier()
  print(training_data.to_string(), end='\n')
  print(trained_models_scores.to_string(), end='\n')
  print(ranking_data.to_string(), end='\n')

  # statistics.nemenyi_friedman(trained_models_scores)
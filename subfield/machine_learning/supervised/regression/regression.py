# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.metrics import mean_absolute_error

from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.svm import SVR
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPRegressor

from techniques.pre_processing.pre_processing import PreProcessor
from techniques.statistics.statistics import Statistics

from types import SimpleNamespace

sns.set()

"""
#Analytics

##Models

###Supervised Learning
- Simple Linear Regression
- Polynomial Linear Regression
- Decision Trees
- Random Forests
- Suport Vector Regressor
- Artificial Neural Networks
- Logistic Regression
  - Implementar
"""

class RegressionModel():

  def __init__(self, processed_data):
    self.X = processed_data.X
    self.y = processed_data.y.reshape(1, -1)[0]
    self.scaler_X = processed_data.scaler_X
    self.scaler_y = processed_data.scaler_y
    self.train_validation_test_indices = processed_data.train_validation_test_indices
    self.n_tests = processed_data.n_tests
    self.test_splits = processed_data.test_splits
    self.regressors_models = []
    self.trained_models = {}
    self.training_data = {}
    return
  
  def _map_dict(self, original_dict, map_by):
    return { model: list(map(lambda d: d[map_by], original_dict[model]))
                    for model in original_dict.keys() }

  def simple_linear_regressor(self):
    model = {
      'regressor': LinearRegression(n_jobs=-1),
      'name': 'Simple Linear Regressor'
    }
    return SimpleNamespace(**model)
  
  def polynomial_linear_regressor(self, degree=2):
    poly = PolynomialFeatures(degree=degree)
    self.X_poly = poly.fit_transform(self.X)

    model = {
      'regressor': LinearRegression(),
      'name': 'Polynomial Linear Regressor'
    }
    return SimpleNamespace(**model)
  
  def decision_tree_regressor(self, criterion='mse'):
    model = {
      'regressor': DecisionTreeRegressor(criterion=criterion),
      'name': 'Decision Tree Regressor'
    }
    return SimpleNamespace(**model)
  
  def random_forest_regressor(self, n_estimators=10, criterion='mse'):
    model = {
      'regressor': RandomForestRegressor(n_estimators=n_estimators, criterion='mse'),
      'name': 'Random Forest Regressor'
    }
    return SimpleNamespace(**model)
  
  def suport_vector_regressor(self, kernel='rbf', degree=3, gamma='scale'):
    model = {
      'regressor': SVR(kernel=kernel, degree=degree, gamma=gamma),
      'name': 'Suport Vector Regressor'
    }
    return SimpleNamespace(**model)
  
  def multilayer_perceptron_regressor(
    self,
    solver='adam',
    max_iter=10000,
    tol=0.00001,
    hidden_layer_sizes=(100),
    learning_rate_init=0.001,
    activation='relu'):
    """
    In regression's problems we only use one neuron in the output layer
    and that neuron must have a linear activation function.
    """

    model = {
      'regressor': MLPRegressor(
        solver=solver,
        max_iter=max_iter,
        tol=tol,
        hidden_layer_sizes=hidden_layer_sizes,
        learning_rate_init=learning_rate_init,
        activation=activation
      ),
      'name': 'Multilayer Perceptron Regressor'
    }
    return SimpleNamespace(**model)
  
  def logistic_regressor(self, solver='liblinear'):
    """
    Used in non-linear problems. Usually in problems with two categories
    """

    model = {
      'regressor': LogisticRegression(solver=solver),
      'name': 'Logistic Regressor'
    }
    return SimpleNamespace(**model)

  def train_models(self):
    for model in self.regressors_models:
      self.trained_models[model.name] = []

      for i in range(self.n_tests):
        for train_index, validation_index, test_index in self.train_validation_test_indices:
          model.regressor.fit(self.X[train_index], self.y[train_index])
          train_score = model.regressor.score(self.X[train_index], self.y[train_index])
          test_score = model.regressor.score(self.X[test_index], self.y[test_index])
          
          y_test = self.scaler_y.inverse_transform(self.y[test_index])
          
          predictions = model.regressor.predict(self.X[test_index])
          predictions = self.scaler_y.inverse_transform(predictions)

          self.trained_models[model.name].append({
            'regressor': model.regressor,
            'score': round((train_score + test_score)/2, 4),
            'mae': round(mean_absolute_error(y_test, predictions), 4)
          })
        print(f'{model.name}: {(i+1)*self.test_splits}/{self.n_tests*self.test_splits}')

      self.training_data[model.name] = {
        #'best_model': np.argmin(list(map(lambda td: td['mae'], self.trained_models[model.name]))),
        'best_model': np.argmax(list(map(lambda td: td['score'],self.trained_models[model.name]))),
        'mean_score': round(np.mean(list(map(lambda td: td['score'], self.trained_models[model.name]))), 4),
        'mean_mae': round(np.mean(list(map(lambda td: td['mae'], self.trained_models[model.name]))), 4)
      }
    
    self.training_data = pd.DataFrame(self.training_data)
    return self.trained_models, self.training_data
  
  def rank_models(self):
    self.trained_models_scores = pd.DataFrame(self._map_dict(self.trained_models, 'score'))
    self.ranking_data = pd.DataFrame(self._map_dict(self.trained_models, 'score'))

    for i in range(self.ranking_data.shape[0]):
      row = self.ranking_data.iloc[i, :]
      sorted_row = sorted(row, reverse=True)
      self.ranking_data.iloc[i, :] = [sorted_row.index(item) + 1 for item in row]
    return self.trained_models_scores, self.ranking_data
  
  def get_best_model(self):
    ranking_mean = np.asarray(self.ranking_data.mean())
    best_ranked_model = np.where(ranking_mean == min(ranking_mean))[0][0]
    model_name = self.ranking_data.columns[best_ranked_model]
    best_model_index = int(self.training_data[model_name]['best_model'])
    return self.trained_models[model_name][best_model_index]
  
  def plot_model(self, regressor, chart_settings):
    X = self.X
    y = self.y

    if self.X.shape == self.y.shape:
      if 'Decision Tree' in regressor.name or 'Random Forest' in regressor.name:
        X = np.arange(min(X), max(X), 0.1)
        X = X.reshape(-1, 1)
      
      plt.scatter(X, y, color='blue')
      plt.plot(X, regressor.predict(X), color='red')
      plt.title(regressor['name'])
      plt.xlabel(chart_settings['x_label'])
      plt.ylabel(chart_settings['y_label'])
    
    else:
      print(f'Unable to plot because X.shape is {X.shape} and y.shape is {y.shape}')
    return


class ThisPreProcessor(PreProcessor):

  def __init__(self, raw_data):
    super().__init__(raw_data)


"""
Main
"""
if __name__ == '__main__':
  # Load Data
  raw_data = pd.read_csv('data/house_prices.csv')
  print(raw_data.describe(include='all'), end='\n')

  # Preprocessing
  pp = ThisPreProcessor(raw_data)

  ## Data cleansing
  pp.drop_unnecessary_data(columns_names=['id', 'date'], axis_name='column')
  pp.rearrange_data(columns_names=[
    'bedrooms', 'bathrooms', 'sqft_living', 'sqft_lot', 'floors', 'waterfront', 'view',
    'condition', 'grade', 'sqft_above', 'sqft_basement', 'yr_built', 'yr_renovated',
    'zipcode', 'lat', 'long', 'sqft_living15', 'sqft_lot15', 'price'
  ])
  pp.check_missing_values()
  pp.deal_with_missing_values()
  # pp.check_inconsistent_values()
  # pp.deal_with_inconsistent_values()
  # pp.check_duplicate_values()
  # pp.deal_with_duplicate_values()
  # pp.check_outliers(plot_columns=pp.raw_data.columns)
  pp.deal_with_outliers()
  # pp.format_data()

  ## Feature Construction
	# pp.create_new_feature()

  ## Feature Extraction
  # pp.reduce_dimensionality(technique='pca', n_components=None)
  
  ## Instances Selection and Partitioning
  pp.split_dataset_into_X_and_y()
  pp.set_train_validation_test_indices(kfold_strategy='KFold',
																			n_tests=10,
																			test_split_size=0.1,
																			validation_split_size=0)

  ## Feature Tuning
  pp.standardize_data()
  # pp.one_hot_encode_data()

  ## Feature Selection
  statistics = Statistics(pp)
  statistics.check_correlation_between_variables()
  statistics.evaluate_feature_selection()
  # pp.select_features()

  """
  #Analysis

  - Use data to create reports and dashboards to gain business insights
  - Extract info and present it in form of: metrics, KPIs, reports, dashboards, etc.
  - Use PowerBI/Tableau/Google Data Studio
  """

  regressor = RegressionModel(pp)
  regressor.regressors_models = [
    regressor.simple_linear_regressor(),
    # regressor.polynomial_linear_regressor(degree=5),
    # regressor.decision_tree_regressor(criterion='mse'),
    # regressor.random_forest_regressor(n_estimators=10, criterion='mse'),
    # regressor.suport_vector_regressor(kernel='rbf', degree=3, gamma='scale'), # to use 'rbf' is necessary to scale the data
    # regressor.multilayer_perceptron_regressor(
    #   solver='adam',
    #   max_iter=10000,
    #   tol=0.00001,
    #   hidden_layer_sizes=(100),
    #   learning_rate_init=0.001,
    #   activation='relu')
    # regressor.logistic_regressor()
  ]
  trained_models, training_data = regressor.train_models()
  trained_models_scores, ranking_data = regressor.rank_models()
  model = regressor.get_best_model()

  print(training_data, end='\n')
  print(trained_models_scores, end='\n')
  print(ranking_data, end='\n')

  statistics.nemenyi_friedman(trained_models_scores)

  chart_settings = {
    'x_label': 'X',
    'y_label': 'Y'
  }
  regressor.plot_model(model['regressor'], chart_settings)
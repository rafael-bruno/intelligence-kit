# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from apyori import apriori

from techniques.pre_processing.pre_processing import PreProcessor
from techniques.statistics.statistics import Statistics

from types import SimpleNamespace

sns.set()

"""
#Analytics

##Models

###Unsupervised Learning
- Apriori
"""

class AssociationModel():

  def __init__(self, processed_data):
    self.X = processed_data.X
    self.associators_models = []
    return

  def apriori_associator(self,
                        min_support=0.003,
                        min_confidence=0.2,
                        min_lift=3.0,
                        min_length=2):
    model = {
      'associator': apriori(self.X,
                            min_support=min_support,
                            min_confidence=min_confidence,
                            min_lift=min_lift,
                            min_length=min_length),
      'name': 'Apriori Associator'
    }
    return SimpleNamespace(**model)
  
  def rank_models(self):
    return

  def get_best_model(self):
    return
  
  def print_report(self, associator, report_settings):
    rules = [list(r) for r in list(associator)]
    filtered_rules = [[list(x) for x in rules[i][2]] for i in range(report_settings['n_rules'])]
    
    print('BEST RULES')
    for i, rule in enumerate(filtered_rules):
      rule = rule[0]
      print(f'{(i + 1):2}) THERE\'S a {rule[2]:4.2} CHANCE of', end=' ')
      for j, item in enumerate(rule[1]):
        if j == 0:
          print(f'{item:20}', end='')
        else:
          print(f' AND {item}', end='')

      print(f'be {rule[3]:.2}x more frequent WHEN', end=' ')

      for j, item in enumerate(rule[0]):
        if j == 0:
          print(f'occurs {item}', end='')
        else:
          print(f' AND {item}', end='')
      print()
    return


class ThisPreProcessor(PreProcessor):

  def __init__(self, raw_data):
    super().__init__(raw_data)
  
  def split_dataset(self):
    self.X = [[str(raw_data.values[i, j]) for j in range(self.n_columns)] for i in range(self.n_rows)]

"""
Main
"""
if __name__ == '__main__':
  #Load Data
  raw_data = pd.read_csv('data/market.csv')
  print(raw_data.describe(include='all'), end='\n')

  # Preprocessing
  pp = ThisPreProcessor(raw_data)

  ## Data cleansing
  pp.drop_unnecessary_data()
  # pp.rearrange_data()
  pp.check_missing_values()
  # pp.deal_with_missing_values()
  # pp.check_inconsistent_values()
  # pp.deal_with_inconsistent_values()
  # pp.check_duplicate_values()
  # pp.deal_with_duplicate_values()
  # pp.check_outliers(plot_columns=pp.raw_data.columns)
  # pp.deal_with_outliers()
  # pp.format_data()

  ## Feature Construction
	# pp.create_new_feature()

  ## Feature Extraction
  # pp.reduce_dimensionality(technique='pca', n_components=None)
  
  ## Instances Selection and Partitioning
  pp.split_dataset()

  ## Feature Tuning
  # pp.standardize_data()
  # pp.one_hot_encode_data()

  ## Feature Selection
  statistics = Statistics(pp)
  statistics.check_correlation_between_variables()
  statistics.evaluate_feature_selection()
  # pp.select_features()

  """
  #Analysis

  - Use data to create reports and dashboards to gain business insights
  - Extract info and present it in form of: metrics, KPIs, reports, dashboards, etc.
  - Use PowerBI/Tableau/Google Data Studio
  """

  associator = AssociationModel(pp)
  associator.associators_models = [
    associator.apriori_associator(min_support=0.003,
                                  min_confidence=0.2,
                                  min_lift=3.0,
                                  min_length=2)
  ]
  report_settings = {
    'n_rules': 10
  }
  associator.print_report(associator.associators_models[0].associator, report_settings)
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import DBSCAN

from sklearn.preprocessing import StandardScaler

from techniques.pre_processing.pre_processing import PreProcessor
from techniques.statistics.statistics import Statistics

from types import SimpleNamespace

sns.set()

"""
#Analytics

##Models

###Unsupervised Learning
- KMeans
- Agglomerative Clustering
- DBSCAN
"""

class ClusteringModel():

  def __init__(self, processed_data):
    self.X = processed_data.X
    self.scaler_X = processed_data.scaler_X
    self.clustering_models = []
    self.built_models = {}
    self.colors = ['red', 'blue', 'green', 'yellow', 'orange', 'purple', 'gray']
    return

  def k_means_clustering(self, n_clusters):
    model = {
      'clusterizator': KMeans(n_clusters=n_clusters),
      'n_clusters': n_clusters,
      'name': 'K-Means Clustering'
    }
    return SimpleNamespace(**model)
  
  def agglomerative_clustering(self, n_clusters=None, affinity='euclidean', linkage='ward'):
    model = {
      'clusterizator': AgglomerativeClustering(
        n_clusters=n_clusters,
        affinity=affinity,
        linkage=linkage
      ),
      'n_clusters': n_clusters,
      'name': 'Agglomerative Clustering'
    }
    return SimpleNamespace(**model)
  
  def dbscan(self, eps, min_samples):
    """
    Density-Based Spatial Clustering of Applications with Noise

    Vantagens: não é necessário especificar o número de clusters; em geral,
    apresenta melhores resultados que o k-means; mais rápido que o k-means
    e que o agrupamento hierárquico; encontra padrões não-lineares;
    robusto contra outliers

    Desvantagens: é mais trabalhoso do que o k-means. O desafio desse
    algortimo é encontrar os melhores valores do epsilon e do min_samples
    """

    model = {
      'clusterizator': DBSCAN(eps=eps, min_samples=min_samples),
      'n_clusters': 4,
      'name': 'DBSCAN'
    }
    return SimpleNamespace(**model)

  def check_n_clusters(self):
    wcss = []
    for i in range(1,11):
      kmeans = KMeans(n_clusters=i, random_state=0)
      kmeans.fit(self.X)
      wcss.append(kmeans.inertia_)

    plt.plot(range(1,11), wcss)
    plt.xlabel('Number of clusters')
    plt.ylabel('WCSS')
    plt.show()
    return

  def build_and_predict(self):
    for model in self.clustering_models:
      print(f'Building and predicting {model.name} ...')
      self.built_models[model.name] = {
        'clusterizator': model.clusterizator,
        'predictions': model.clusterizator.fit_predict(self.X)
        # 'centroids': model.clusterizator.cluster_centers_,
        # 'labels': model.clusterizator.labels_
      }
    
    self.built_models = pd.DataFrame(self.built_models)
    return self.built_models
  
  def plot_model(self, clusterizator_name, chart_settings):
    clusterizator = self.built_models[clusterizator_name]
    plt.figure()
    [plt.scatter(self.X[clusterizator.predictions == i, 0],
                 self.X[clusterizator.predictions == i, 1],
                 s=100,
                 c=self.colors[i],
                 label='Cluster ' + str(i+1))
      for i in range(clusterizator.clusterizator.n_clusters)]
    plt.title(clusterizator_name)
    plt.xlabel(chart_settings['x_label'])
    plt.ylabel(chart_settings['y_label'])
    plt.legend()
    plt.show()
    return


class ThisPreProcessor(PreProcessor):

  def __init__(self, raw_data):
    super().__init__(raw_data)
  
  def create_new_feature(self) -> pd.DataFrame:
    self.raw_data['BILL_TOTAL'] = self.raw_data[['BILL_AMT1', 'BILL_AMT2', 'BILL_AMT3', 'BILL_AMT4', 'BILL_AMT5', 'BILL_AMT6']].astype(float).sum(axis=1)
    self.raw_data['PAY_TOTAL'] = self.raw_data[['PAY_AMT1', 'PAY_AMT2', 'PAY_AMT3', 'PAY_AMT4', 'PAY_AMT5', 'PAY_AMT6']].astype(float).sum(axis=1)
    return self.raw_data


"""
Main
"""
if __name__ == '__main__':
  #Load Data
  raw_data = pd.read_csv('data/credit_card_data.csv')
  print(raw_data.describe(include='all'), end='\n')
  print(raw_data.head())
  raw_data = raw_data.rename(raw_data.iloc[0, :], axis='columns')

  # Preprocessing
  pp = ThisPreProcessor(raw_data)

  pp.drop_unnecessary_data(columns_names=['ID'], axis_name='row')

  ## Feature Construction
  pp.create_new_feature()

  ## Data cleansing
  pp.rearrange_data(columns_names=[
    'LIMIT_BAL', 'SEX', 'EDUCATION', 'MARRIAGE', 'AGE', 'PAY_0', 'PAY_2', 'PAY_3', 'PAY_4',
    'PAY_5', 'PAY_6', 'BILL_AMT1', 'BILL_AMT2', 'BILL_AMT3', 'BILL_AMT4', 'BILL_AMT5',
    'BILL_AMT6', 'BILL_TOTAL', 'PAY_AMT1', 'PAY_AMT2', 'PAY_AMT3', 'PAY_AMT4',
    'PAY_AMT5', 'PAY_AMT6', 'PAY_TOTAL', 'default payment next month'
  ])
  pp.check_missing_values()
  pp.deal_with_missing_values()
  # pp.check_inconsistent_values()
  # pp.deal_with_inconsistent_values()
  # pp.check_duplicate_values()
  # pp.deal_with_duplicate_values()
  # pp.check_outliers(plot_columns=pp.raw_data.columns)
  # pp.deal_with_outliers()
  # pp.format_data()

  ## Feature Extraction
  # pp.reduce_dimensionality(technique='pca', n_components=None)
  
  ## Instances Selection and Partitioning
  pp.split_dataset_into_X_and_y(X_columns_names=['LIMIT_BAL', 'BILL_TOTAL']) # 'LIMIT_BAL', 'SEX', 'EDUCATION', 'MARRIAGE', 'AGE', 'BILL_TOTAL'

  ## Feature Tuning
  pp.standardize_data()
  # pp.one_hot_encode_data()

  ## Feature Selection
  statistics = Statistics(pp)
  statistics.check_correlation_between_variables()
  statistics.evaluate_feature_selection()
  # pp.select_features()

  """
  #Analysis

  - Use data to create reports and dashboards to gain business insights
  - Extract info and present it in form of: metrics, KPIs, reports, dashboards, etc.
  - Use PowerBI/Tableau/Google Data Studio
  """

  clusterizator = ClusteringModel(pp)
  clusterizator.check_n_clusters()

  clusterizator.clustering_models = [
    clusterizator.k_means_clustering(n_clusters=4),
    # clusterizator.agglomerative_clustering(n_clusters=4, affinity='euclidean', linkage='ward'),
    # clusterizator.dbscan(eps=0.37, min_samples=4)
  ]
  built_models = clusterizator.build_and_predict()

  # statistics.nemenyi_friedman()

  chart_settings = {
    'x_label': 'Limite',
    'y_label': 'Gastos'
  }

  clusterizator.plot_model('K-Means Clustering', chart_settings)
  # clusterizator.plot_model('Agglomerative Clustering', chart_settings)
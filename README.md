# intelligence-kit

Generic codes to solve different intelligence related problems

## Libraries

pip install apyori matplotlib numpy pandas pyod scikit-posthocs scipy sklearn seaborn statsmodels

## MODULES

### Techniques
- Heuristics (to do)
- Meta-heuristics (to do)
- Preprocessing
- Statistics (to improve)

### Subfield

#### Big Data
- (to do)

#### Computer Vision
- (to do)

#### Evolutionary Computation
- (to do)

#### Machine Learning

##### Deep Learning
- (to do)

##### Supervised Learning
- Classification
- Regression

##### Unsupervised Learning
- Association
- Clustering

##### Reinforcement Learning
- (to do)

#### Natural Language Processing
- (to do)

#### Operational Research
- (to do)

#### Quantum Computing
- (to do)
